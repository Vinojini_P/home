---
title: "3.X.X"
weight: 1
---

* [version 3.8.0](https://entgra-documentation.gitlab.io/v3.8.0/) - **_7 November 2019_**
* [version 3.7.0](https://entgra-documentation.gitlab.io/v3.7.0/) - **_26 October 2019_**
* [version 3.6.0](https://entgra.atlassian.net/wiki/spaces/IoTS360/overview) - **_9 July 2019_**
* [version 3.5.0](https://entgra.atlassian.net/wiki/spaces/IoTS350/overview) - **_24 April 2019_**
* [version 3.4.0](https://entgra.atlassian.net/wiki/spaces/IoTS340/overview) - **_11 November 2018_**




